/* 
 * File:   BitMatrix2D.h
 * Author: thegiallo
 *
 * Created on 28 novembre 2011, 13.14
 */

#ifndef BITMATRIX2D_H
#define	BITMATRIX2D_H

#include <stdint.h>
#include <sys/types.h>

#define SIZEOFBLOB ((width*height+7)>>3)

class BitMatrix2D
{
public:
    BitMatrix2D();
    BitMatrix2D(unsigned int width, unsigned int height);
    BitMatrix2D(const BitMatrix2D& orig);
    virtual ~BitMatrix2D();
    
    bool initBlob(unsigned int width, unsigned int height);
    bool initBlob(unsigned int width, unsigned int height, uint_fast8_t* blob);
    bool loadFrom(unsigned int width, unsigned int height, uint_fast8_t* bytes_array);
    bool writeTo(uint_fast8_t* bytes_array);
    bool setAllMatrixTo(bool bit);
    bool getBitAt(unsigned int x, unsigned int y);
    bool setBitAt(unsigned int x, unsigned int y, bool bit);
    unsigned int getWidth();
    unsigned int getHeight();
    unsigned int getSizeOfBlob(void);
	static unsigned int getSizeOfBlob(unsigned int width, unsigned int height);
    uint_fast8_t* blob;
private:
    unsigned int width, height;
};

#endif	/* BITMATRIX2D_H */

