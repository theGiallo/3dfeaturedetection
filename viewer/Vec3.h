/* 
 * File:   Vec3.h
 * Author: thegiallo
 *
 * Created on 28 gennaio 2011, 0.05
 */

#ifndef VEC3_H
#define	VEC3_H

#include <cstdint>

class Vec3;
class Vec3_int;

#include "Quaternion.h"

#ifndef X_COMPONENT
#define X_COMPONENT 0
#endif
#ifndef Y_COMPONENT
#define Y_COMPONENT 1
#endif
#ifndef Z_COMPONENT
#define Z_COMPONENT 2
#endif

class Vec3
{
public:
    float x,y,z;

    Vec3();
    Vec3(float x, float y, float z);
    float getModule(void);
    float getSin(Vec3 v);
    float getCos(Vec3 v);
    void normalize(void);
    Vec3 getNormalized(void);
    void add(Vec3 v);
    Vec3 operator +(Vec3 v);
    Vec3 operator +=(Vec3 v);
    void sub(Vec3 v);
    Vec3 operator -(Vec3 v);
    Vec3 operator -=(Vec3 v);
    void add(uint8_t i, float f); /// adds as if it was an array {x,y,z}
    void add(float k);
    Vec3 operator +(float k);
    Vec3 operator +=(float k);
    void sub(float k);
    Vec3 operator -(float k);
    Vec3 operator -=(float k);
    void mult(uint8_t i, float f); /// multiplies as if it was an array {x,y,z}
    void mult(float k);
    Vec3 operator *(float k);
    Vec3 operator *=(float k);
    void div(uint8_t i, float f); /// divides as if it was an array {x,y,z}
    void div(float k);
    Vec3 operator/(float k);
    Vec3 operator/=(float k);
    float scalar(Vec3 v);
    float operator *(Vec3 v);
    Vec3 getVectorial(Vec3 v);
    Vec3 operator ^(Vec3 v);
    Vec3 operator ^=(Vec3 v);
    bool isEqual(Vec3 v);
    bool operator ==(Vec3 v);
    bool operator !=(Vec3 v);
    void clone(Vec3 v);
    Vec3 getClone(void);
    Vec3 operator =(Vec3 v);
    float get(uint8_t i) const; /// returns as if it was an array {x,y,z}
    float getX(void) const;
    float getY(void) const;
    float getZ(void) const;
    void set(uint8_t i, float f); /// sets as if it was an array {x,y,z}
    void setX(float x);
    void setY(float y);
    void setZ(float z);
    void print(void);
    void rotate(Quaternion q, Vec3 center = Vec3());
    Vec3 getRotated(Quaternion q, Vec3 center = Vec3());
    void scale(Vec3 s, Vec3 center = Vec3());
    Vec3 getScaled(Vec3 s, Vec3 center = Vec3());
    
    operator Vec3_int();
};

class Vec3_int
{
public:
    int x,y,z;
    
    Vec3_int();
    Vec3_int(int x, int y, int z);
    Vec3_int(int* xyz);
    
    operator Vec3();
    Vec3_int operator +(Vec3_int v);
    Vec3_int operator +=(Vec3_int v);
    Vec3_int operator -(Vec3_int v);
    Vec3_int operator -=(Vec3_int v);
    bool operator ==(Vec3_int v);
    bool operator !=(Vec3_int v);
    
    void print(void);
    
};


#endif	/* VEC3_H */

