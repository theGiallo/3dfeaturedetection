#ifdef __APPLE__
#include <glut.h>
#else
#include <GL/glut.h>
#endif

#define EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#include <Eigen/Sparse>
// #include <unsupported/Eigen/CholmodSupport>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <list>

#include <sstream>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include "Vec3.h"
#include "BitMatrix2D.h"

#define ABS(a) ((a)>=0?(a):-(a))
#define SIGN(a) ((a)>=0?1.0:-1.0)

#define CURV_MINCURV 0
#define CURV_MAXCURV 1
#define CURV_MINDIRX 2
#define CURV_MINDIRY 3
#define CURV_MINDIRZ 4
#define CURV_MAXDIRX 5
#define CURV_MAXDIRY 6
#define CURV_MAXDIRZ 7
#define CURV_COLOR_RED 8
#define CURV_COLOR_GREEN 9
#define CURV_COLOR_BLUE 10
#define CURV_BITFIELD_DATA 11
#define CURV_DATA_NUM 12

#define UTILITYBITS_NUM 2

struct CBF
{
	unsigned int is_maximum:1;
	unsigned int type:3;
	unsigned int utilitybit1:1;
	unsigned int utilitybit2:1;
};

typedef enum VertexCurvType_enum
{
	CONVEX_ELLIPTICAL, /// ++
	CONCAVE_ELLIPTICAL, /// --
	HYPERBOLIC, /// SADDLE +- -+
	CONVEX_CYLINDRICAL, /// 0+ +0
	CONCAVE_CYLINDRICAL, /// 0- -0
	PLANAR, /// 00
	VERTEX_CURVATURE_TYPE_COUNT
} VertexCurvType;

typedef enum ColorRenderType_enum
{
	BASE_COLOR,
	CURVATURE_COLOR,
	VERTEX_TYPE_COLOR,
	COLOR_RENDER_TYPE_COUNT
} ColorRenderType;

using namespace Eigen;
using namespace std;

class VF
{
public:
    	
// #define VF_DRAW_MODE_NUM 5
//     typedef enum {SMOOTH, POINTS, WIRE, HIDDEN, FLAT} draw_mode_t;
//#define VF_DRAW_MODE_NUM 4
    typedef enum {SMOOTH, FLAT, POINTS, WIRE, VF_DRAW_MODE_NUM} draw_mode_t;

////////////////////////////////////////////////////////////////////////////////
/////////////////// METHOD DECLARATIONS ////////////////////////////////////////
    VF();
    ~VF();

/////////////////// I/O ////////////////////////////////////////////////////////
    void read(char *str);
    void write(char *str);
    void readOBJ(const char *str);
    void writeOBJ(const char *str);
    void readOFF (char* meshfile);
    void writeOFF (char *fname);
    
/////////////////// Curvature //////////////////////////////////////////////////
	VertexCurvType getVertexCurvType(int id, int radius);
	void calcVertexCurvType(void);
	Vec3& getBilinearInterpolatedColor(float k1, float k2, Vec3 C0, Vec3 C1, Vec3 C2, Vec3 C3, Vec3 &res);
	float colorScale(float curvature);
	Vec3& getColorFromScalarField(float max_curv, float min_curv, Vec3& res);
	const GLfloat* getColorForVertexCurvType(VertexCurvType vct);
	bool isMax(float* vertex, float* to_compare);
	int getNonMaximaSuppressionRadius(int radius_id);
	void nonMaximaSuppression(int radius, VertexCurvType vct=VERTEX_CURVATURE_TYPE_COUNT);
	
/////////////////// Topology ///////////////////////////////////////////////////
    bool isManifold();
    void initTT();
    inline Vector3i getTT(int i);
    VectorXi getVT(int i);
    VectorXi getVV(int i);
    VectorXi getRadialVV(int i, int radius);
    void nearestVertex(const Vec3& point, int& vertex_id);
    
/////////////////// OpenGL /////////////////////////////////////////////////////
	bool setNonBaseColor(int c_radius, int v_id, ColorRenderType color_type);
	void setBaseColor(void);
    void draw (draw_mode_t mode, bool light=true);
    void bb ();

/////////////////// END METHOD DECLARATIONS ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

public:
   
////////////////////////////////////////////////////////////////////////////////
/////////////////// CLASS FIELDS ///////////////////////////////////////////////
	
    double center[3];
    double diagonal;
    
	/**
	 * Nr = number of different radiuses
	 * Nv = number of vertices
	 * curv = 8 x float (mincurv maxcurv mindirx mindiry mindirz maxdirx maxdiry maxdirz)
	 * curv_rad = Nv x curv (curvatures at the same radius)
	 * curvatures = Nr x curv_rad
	 *
	 * curvatures[i] = all curvature data per vertex of radius i
	 * curvatures[i][j] = curvature data of radius i of the jth vertex
	 **/
	float*** curvatures;
	int* radiuses;
	BitMatrix2D non_maxima_suppression_calculated;
	int radiuses_num;
	int active_curvature;
	ColorRenderType color_type;
    static float curv_eps;
    
    bool render_features;
    bool render_nonmaximasuppressed;
    
    bool render_features_of_type[VERTEX_CURVATURE_TYPE_COUNT];
    
    float NMS_radius_factor;
    
    Vec3 test_picked_point;
    bool render_radius_test;
    GLdouble *modelview_matrix;
    
    BitMatrix2D utilitybits[UTILITYBITS_NUM];
	
    // #Vx3: Stores the vertex coordinates, one vertex per row
    MatrixXd V;
    
    // #Fx3[4]: in the ith row, stores the indices of the vertices of the ith face
    MatrixXi F;
    
    // #Fx3: Stores the Triangle-Triangle relation
    MatrixXi TT;
    
    // #V: Stores partial Vertex-Triangle relation
    VectorXi VT;

	// #V: index attribute for vertices
	VectorXi IV;
	
	
	// directory containing curvature files
	std::string curvatures_dir_path;
	
	static GLfloat base_color_3f[3];// generic mesh color
	
private:
    bool TTVTflag;  // true iff TV* and TT are up-to-date

/////////////////// END CLASS FIELDS ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
};

