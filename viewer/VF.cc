#include "VF.hh"
#include <cfloat>

	
    float VF::curv_eps = 0.1f;
    GLfloat VF::base_color_3f[3] = {0.5f,0.5f,1.0f}; 

    VF::VF()
    {
    	curvatures = NULL;
    	radiuses = NULL;
    	radiuses_num = 0;
		active_curvature = -1;
		color_type = BASE_COLOR;
		render_features = false;
		render_nonmaximasuppressed = false;
		render_radius_test = false;
		curvatures_dir_path = "";
		NMS_radius_factor = 1.414f;
		for (int i=0 ; i<VERTEX_CURVATURE_TYPE_COUNT ; i++)
		{
			render_features_of_type[i] = true;
		}
    };
    VF::~VF()
    {
    	free(curvatures);
    	delete [] radiuses;
    };
    

////////////////////// I/O ///////////////////////////////////////

    // read mesh from an ascii file with automatic detection of file
    // format. supported: obj, off)
    void VF::read(char *str)
    {
		char* p;
		for (p = str; *p != '\0'; p++) ;
		while (*p != '.') p--;

		//printf ("%s\n", p);
		if (!strcmp(p, ".obj") || !strcmp(p, ".OBJ"))
		{
			//printf ("readOBJ\n");
			readOBJ (str);
		} else if (!strcmp(p, ".off") || !strcmp(p, ".OFF"))
		{
			//printf ("readOFF\n");
			readOFF (str);
		} else {
            fprintf (stdout, "File format not supported!\n");
            return;
        }

        // topology is NOT initialized
        // initTT();
        TTVTflag = false;
        
		fprintf (stdout, "Loaded mesh `%s\'.\n#vertices: %d.\n#faces: %d.\n", str, (int)V.rows(), (int)F.rows());
        if (!isManifold()) fprintf (stdout, "Mesh is not edge manifold! Cannot use topology.\n"); 
    }


    // write mesh to an ascii file with automatic detection of file
    // format. supported: obj, off)
    void VF::write(char *str)
    {
	char* p;
        for (p = str; *p != '\0'; p++) ;
        while (*p != '.') p--;

        //printf ("%s\n", p);
        if (!strcmp(p, ".obj") || !strcmp(p, ".OBJ"))
        {
            //printf ("writeOBJ\n");
            return writeOBJ (str);
        } else if (!strcmp(p, ".off") || !strcmp(p, ".OFF"))
        {
            //printf ("writeOFF\n");
            return writeOFF (str);
        } else {
            fprintf (stdout, "File format not supported!\n");
            return;
        }
    }


    // Read a mesh from an ascii obj file
    void VF::readOBJ(const char *str)
    {
        ifstream s(str);
        vector<Vector3d> Vtemp;
        vector<Vector3i> Ftemp;
        char buf[1000];
        while(!s.eof())
        {
            s.getline(buf, 1000);
            if (buf[0] == 'v') // vertex coordinates found
            {
                char v;
                double v1,v2,v3;
                sscanf(buf, "%c %lf %lf %lf",&v,&v1,&v2,&v3);
                Vtemp.push_back(Vector3d(v1,v2,v3));
            }
            else if (buf[0] == 'f') // face description found
            {
                char v;
                int v1,v2,v3;
                sscanf(buf, "%c %d %d %d",&v,&v1,&v2,&v3);
                Ftemp.push_back(Vector3i(v1-1,v2-1,v3-1));
            }
        }
        s.close();
        V = MatrixXd(Vtemp.size(),3);
        for(int i=0;i<V.rows();++i) V.row(i) = Vtemp[i];
        F = MatrixXi(Ftemp.size(),3);
        for(int i=0;i<F.rows();++i) F.row(i) = Ftemp[i];
    }
    
	
    // Write a mesh in an ascii obj file
    void VF::writeOBJ(const char *str)
    {
        ofstream s(str);
        for(int i=0;i<V.rows();++i)
            s << "v " << V(i,0) << " " << V(i,1) << " " << V(i,2) << endl;
        for(int i=0;i<F.rows();++i)
            s << "f " << F(i,0)+1 << " " << F(i,1)+1 << " " << F(i,2)+1 << endl;
        s.close();
    }

	
    // read mesh from a ascii off file
    void VF::readOFF (char* meshfile)
    {
        int vnum, fnum;
        FILE *fp = fopen (meshfile, "r");

        if (!fp) fprintf (stderr, "readOFF(): could not open file %s", meshfile);

        int r = 0;
        r = fscanf (fp, "OFF\n%d %d 0\n",  &vnum, &fnum);
        radiuses_num = 0;
        
		boost::filesystem::path mesh_file = boost::filesystem::path(meshfile), curv_path(mesh_file.replace_extension());
		std::string fn = curv_path.filename().string();
		fn.append("_r");
		typedef vector<boost::filesystem::path> vec;             // store paths,
        vec v;                                // so we can sort them later
//        boost::filesystem::path curvatures_path = mesh_file.parent_path();
//        curvatures_path /= boost::filesystem::path("../../curvatures"); 		// TODO move this variable to be a class variable and make setter
		boost::filesystem::path curvatures_path(curvatures_dir_path);
        std::copy(boost::filesystem::directory_iterator(boost::filesystem::path(curvatures_path)), boost::filesystem::directory_iterator(), std::back_inserter(v));
        std::sort(v.begin(), v.end());             // sort, since directory iteration
                                              // is not ordered on some file systems
      	boost::cmatch what;
      	std::stringstream ss;
      	ss<<"^("<<fn<<")([0-9]+)(.curv)$";
      	std::string expr;
      	ss>>expr;
		boost::regex expression(expr);
		
		vec curvature_files_paths;
		std::vector<int> radiuses;
		// find curvature files
        for (vec::const_iterator it (v.begin()); it != v.end(); ++it)
        {
			if (!is_directory(*it))
			{
				if (regex_match((*it).filename().string().c_str(), what, expression))
				{
					std::cout<<*it<<std::endl;
					std::cout<<what[2]<<std::endl;
					curvature_files_paths.push_back(*it);
			      	std::stringstream ss_2;
			      	ss_2<<what[2];
			      	int radius;
			      	ss_2>>radius;
					radiuses.push_back(radius);
					radiuses_num++;
				} else
				{
//					std::cout<<"NO "<<(*it).string()<<std::endl;
				}
			} else
			{
//				std::cout<<"dir "<<(*it).string()<<std::endl;
			}
        }
        
        // allocate the curvatures data
        this->radiuses = new int[radiuses_num];
        non_maxima_suppression_calculated = BitMatrix2D(radiuses_num,1);
        non_maxima_suppression_calculated.setAllMatrixTo(false);
    	curvatures = (float***) malloc(radiuses_num*
												  	//curv_rad*		//curv_rad
													(sizeof(float**)+vnum*
												  		//curv*			//curv
														(sizeof(float*)+CURV_DATA_NUM*sizeof(float))
													)
									);
    	void** curv_rad_arr = (void**)&curvatures[radiuses_num];
//    	unsigned int curv_rad_arr_size = vnum*(sizeof(float*)+CURV_DATA_NUM*sizeof(float));
    	for (int i=0 ; i<radiuses_num ; i++)
    	{
    		curvatures[i] = (float**)curv_rad_arr;
    		float** v = curvatures[i];
    		float* curv_data = (float*)&v[vnum];
    		for (int j=0 ; j<vnum ; j++)
    		{
    			v[j] = curv_data;
    			curv_data+=CURV_DATA_NUM;
    		}
    		curv_rad_arr = (void**) curv_data;
    	}
#ifdef DEBUG
    	//### test
    		std::cout<<"DEBUG assignment..."<<std::endl;
    		curvatures[0][0][0] = 5.0f;
    		curvatures[0][2][6] = 6.0f;
    		curvatures[0][3][4] = 14.0f;
    		curvatures[1][1][4] = 4.0f;
    		std::cout<<"5="<<curvatures[0][0][0]<<" 6="<<curvatures[0][2][6]<<" 14="<<curvatures[0][3][4]<<" 4="<<curvatures[1][1][4]<<std::endl;
    	//### /test
#endif
        V = MatrixXd (vnum, 3);
        F = MatrixXi (fnum, 3);
        for (int i=0 ; i<UTILITYBITS_NUM ; i++)
        {
        	utilitybits[i].initBlob(vnum, 1);
        	utilitybits[i].setAllMatrixTo(false);
    	}

        for (int i = 0; i < V.rows(); i++)
            r = fscanf (fp, "%lf %lf %lf\n", &V(i,0), &V(i,1), &V(i,2));    
        for (int i = 0; i < F.rows(); i++)
            r = fscanf (fp, "3 %d %d %d\n", &F(i,0), &F(i,1), &F(i,2));
        for (int i = 0; i < F.rows(); i++)
            for (int j = 0; j < 3; j++)
                if (F(i,j) >= V.rows())
                    fprintf (stderr, "readOFF(): warning vertex: %d"
                             " in face: %d has value: %d, greater than #v: %d\n",
                             j,i,F(i,j),(int)V.rows());
        fclose (fp);
        for (int i=0 ; i<radiuses_num ; i++)
        {
#ifdef DEBUG
			std::cout<<"opening curvature file #"<<i<<std::endl;
			std::cout<<"radius: "<<radiuses[i]<<std::endl;
			std::cout<<curvature_files_paths[i].string()<<std::endl;
#endif
        	//open curvature file
        	fp = fopen (curvature_files_paths[i].string().c_str(),"r");
#ifdef DEBUG
			std::cout<<"fp: "<<fp<<std::endl;
#endif
        	int vnum_curv;
	        r = fscanf (fp, "%d\n", &vnum_curv);
	        if (vnum!=vnum_curv)
	        {
	        	std::cerr<<"file "<<curvature_files_paths[i]<<" contains a number of vertices different from the one of the mesh \""<<meshfile<<"\""<<std::endl;
	        	continue;
	        }
	        // read curvature data
	        for (int j = 0; j < vnum; j++)
	        {
            	r = fscanf (fp, "%f %f %f %f %f %f %f %f\n"	, &curvatures[i][j][0]
        															, &curvatures[i][j][1]
        															, &curvatures[i][j][2]
        															, &curvatures[i][j][3]
        															, &curvatures[i][j][4]
        															, &curvatures[i][j][5]
        															, &curvatures[i][j][6]
        															, &curvatures[i][j][7] ); // yes, it's not dynamic, I know, but it does not need to be so

				// calculate and store representation color
				float* vdata = curvatures[i][j];			
				Vec3 color = getColorFromScalarField(vdata[CURV_MAXCURV], vdata[CURV_MINCURV], color);
				vdata[CURV_COLOR_RED] = color.getX();
				vdata[CURV_COLOR_GREEN] = color.getY();
				vdata[CURV_COLOR_BLUE] = color.getZ();
				
				((struct CBF*)&vdata[CURV_BITFIELD_DATA])->is_maximum = 0x1;
				((struct CBF*)&vdata[CURV_BITFIELD_DATA])->type = getVertexCurvType(j,i);
			}
			std::cout<<curvatures[i][0][0]<<std::endl;
			this->radiuses[i] = radiuses[i];
			//nonMaximaSuppression(i);
	        fclose (fp);
        }
    }

	
    // write mesh to an ascii off file
    void VF::writeOFF (char *fname)
    {
        FILE *fp = fopen (fname, "w");

        if (!fp) fprintf (stderr, "writeOFF(): could not open file %s", fname);
        fprintf (fp, "OFF\n%d %d 0\n",  (int) V.rows(), (int) F.rows());
        for (int i = 0; i < V.rows(); i++)
            fprintf (fp, "%f %f %f\n", V(i,0), V(i,1), V(i,2));
        for (int i = 0; i < F.rows(); i++)
            fprintf (fp, "3 %d %d %d\n", F(i,0), F(i,1), F(i,2));
        fclose (fp);
    }
    
//////////////// Curvature ///////////////////////////
	VertexCurvType VF::getVertexCurvType(int id, int radius)
	{
		float *data = curvatures[radius][id];
		if (data[CURV_MAXCURV] > curv_eps)
		{
			if (data[CURV_MINCURV]>curv_eps)
			{
				return CONVEX_ELLIPTICAL; 	// ++
			} else
			if (data[CURV_MINCURV]<-curv_eps)
			{
				return HYPERBOLIC;			// +-
			} else
			{
				return CONVEX_CYLINDRICAL;	// +0
			}
		} else
		if (data[CURV_MAXCURV] < -curv_eps)
		{
			if (data[CURV_MINCURV]>curv_eps)
			{
				return HYPERBOLIC;			// -+
			} else
			if (data[CURV_MINCURV]<-curv_eps)
			{
				return CONCAVE_ELLIPTICAL;	// --
			} else
			{
				return CONVEX_CYLINDRICAL;	// -0
			}
		} else
		{
			if (data[CURV_MINCURV]>curv_eps)
			{
				return CONVEX_CYLINDRICAL;	// 0+
			} else
			if (data[CURV_MINCURV]<-curv_eps)
			{
				return CONCAVE_CYLINDRICAL;	// 0-
			} else
			{
				return PLANAR;				// 00
			}
		}
	}
	
	void VF::calcVertexCurvType(void)
	{
		for (int r=0 ; r<radiuses_num ; r++)
		{
			for (int i=0 ; i<V.rows() ; i++)
			{
				((struct CBF*)&curvatures[r][i][CURV_BITFIELD_DATA])->type = getVertexCurvType(i,r);
			}
		}
	}

	Vec3& VF::getBilinearInterpolatedColor(float k1, float k2, Vec3 C0, Vec3 C1, Vec3 C2, Vec3 C3, Vec3 &res)
	{
		Vec3 px0 = C0 * (1 - k1) + C1 * k1;
		Vec3 px1 = C3 * (1 - k1) + C2 * k1;
		res = px0 * (1 - k2) + px1 * k2;
		return res;
	}
	
	float VF::colorScale(float curvature)
	{
//		return 1/(1+ABS(curvature))*SIGN(curvature);
//		return atan(curvature/10.0f)/(M_PI/2.0f);
//		float scaleCurvColor = 0.5;
//	    return (1-exp(-(curvature*curvature)/(2*scaleCurvColor*scaleCurvColor)))*SIGN(curvature);
	    return (1-exp(-(curvature*curvature)/(ABS(curvature))))*SIGN(curvature);
	}
	
	Vec3& VF::getColorFromScalarField(float max_curv, float min_curv, Vec3& res)
	{
		static const Vec3 R(1.0f, 0.0f, 0.0f);
		static const Vec3 G(0.0f, 1.0f, 0.0f);
		static const Vec3 B(0.0f, 0.0f, 1.0f);
		static const Vec3 Y(1.0f, 1.0f, 0.0f);// = (R + G);
		static const Vec3 C(0.0f, 1.0f, 1.0f);// = (B + G);
		static const Vec3 W(1.0f, 1.0f, 1.0f);// = (R + B + G);

		Vec3 cr;
		
		max_curv = colorScale(max_curv);
//    	max_curv = (max_curv + M_PI/2.0f)/M_PI;
		min_curv = colorScale(min_curv);
//    	min_curv = (min_curv + M_PI/2.0f)/M_PI;
//    	float e = 0.01f;

		if (max_curv >= 0.0 && min_curv >= 0.0)
			getBilinearInterpolatedColor (max_curv, min_curv, W, Y, R, Y, res);
		if (max_curv >= 0.0 && min_curv < 0.0)
			getBilinearInterpolatedColor (max_curv, -min_curv, W, Y, G, C, res);
		if (max_curv < 0.0 && min_curv >= 0.0)
			getBilinearInterpolatedColor (-max_curv, min_curv, W, C, G, Y, res);
		if (max_curv < 0.0 && min_curv < 0.0)
			getBilinearInterpolatedColor (-max_curv, -min_curv, W, C, B, C, res);
		return res;
	}
	
	const GLfloat* VF::getColorForVertexCurvType(VertexCurvType vct)
	{
		const static GLfloat vct_colors[VERTEX_CURVATURE_TYPE_COUNT][3] = {
			{1.0f, 0.0f, 0.0f}, // CONVEX_ELLIPTICAL
			{0.0f, 0.0f, 1.0f}, // CONCAVE_ELLIPTICAL
			{0.0f, 1.0f, 0.0f}, // HYPERBOLIC
			{1.0f, 1.0f, 0.0f}, // CONVEX_CYLINDRICAL
			{0.0f, 1.0f, 1.0f}, // CONCAVE_CYLINDRICAL
			{0.5f, 0.5f, 0.5f} // PLANAR
			};
		return vct_colors[vct];
	}
	
	bool VF::isMax(float* vertex, float* to_compare)
	{
		struct CBF* v_cbf = ((struct CBF*)&vertex[CURV_BITFIELD_DATA]);
		struct CBF* tc_cbf = ((struct CBF*)&to_compare[CURV_BITFIELD_DATA]);
		assert(v_cbf->type==tc_cbf->type);
		float val1,val2;
		switch((VertexCurvType)v_cbf->type)
		{
		case CONVEX_ELLIPTICAL:
		case CONCAVE_ELLIPTICAL:
		case HYPERBOLIC:
			val1 = 	vertex[CURV_MAXCURV]*vertex[CURV_MAXCURV]+
					vertex[CURV_MINCURV]*vertex[CURV_MINCURV];
			val2 =  to_compare[CURV_MAXCURV]*to_compare[CURV_MAXCURV]+
					to_compare[CURV_MINCURV]*to_compare[CURV_MINCURV];
			break;
		case CONVEX_CYLINDRICAL:
			val1 = 	vertex[CURV_MAXCURV];
			val2 = 	to_compare[CURV_MAXCURV];
			break;
		case CONCAVE_CYLINDRICAL:
			val1 = 	-vertex[CURV_MAXCURV];
			val2 = 	-to_compare[CURV_MAXCURV];
			break;
		case PLANAR:
			val1 = 	-vertex[CURV_MAXCURV]*vertex[CURV_MAXCURV]-
					vertex[CURV_MINCURV]*vertex[CURV_MINCURV];
			val2 =  -to_compare[CURV_MAXCURV]*to_compare[CURV_MAXCURV]-
					to_compare[CURV_MINCURV]*to_compare[CURV_MINCURV];
			break;
		default:
			std::cerr<<"Error: type was "<<v_cbf->type<<std::endl;
			assert(false);
		}
		if (val1==val2)
		{
			return vertex>to_compare;
		}
		return val1>val2;
	}
	
	int VF::getNonMaximaSuppressionRadius(int radius_id)
	{
		return radius_id>=0 ? NMS_radius_factor * radiuses[radius_id]:0;
	}
	
	void VF::nonMaximaSuppression(int radius_id, VertexCurvType vct)
	{
		float **curvature = curvatures[radius_id];
		bool check_all = (vct==VERTEX_CURVATURE_TYPE_COUNT);
		if (check_all)
		{
#ifdef DEBUG
			std::cout<<"	check all"<<std::endl;
#endif
			non_maxima_suppression_calculated.setBitAt(radius_id,0,true);
		}
		int radius = getNonMaximaSuppressionRadius(radius_id);
#ifdef DEBUG
		std::cout<<"	suppressing with radius "<<radius<<std::endl;
#endif
		for (int i=0 ; i<V.rows() ; i++)
		{
			float* data = curvature[i];
			VertexCurvType t=vct;
			if (check_all)
			{
				t = (VertexCurvType)((struct CBF*)&data[CURV_BITFIELD_DATA])->type;
			} else
			if ((VertexCurvType)((struct CBF*)&data[CURV_BITFIELD_DATA])->type!=vct)
			{
				continue;
			}
			VectorXi vv = getRadialVV(i,radius);
//			std::cout<<vv.rows()<<" vertices in radius "<<radius<<" around "<<i<<std::endl;
			for (int vid=0 ; vid<vv.rows() ; vid++)
			{
				float* data_v = curvature[vv(vid)];
				if ( ((VertexCurvType)((struct CBF*)&data_v[CURV_BITFIELD_DATA])->type) != t )
				{
					continue;
				} else
				if (!isMax(data,data_v))
				{
					((struct CBF*)&data[CURV_BITFIELD_DATA])->is_maximum = 0x0;
					assert(((struct CBF*)&data[CURV_BITFIELD_DATA])->is_maximum == 0x0);
					break;
				}
			}
		}
	}
    
	
//////////////// Topology ////////////////////////////	
    
	
    // check if the mesh is edge-manifold
    bool VF::isManifold()
    {
        vector<vector<int> > TTT;
        for(int f=0;f<F.rows();++f)
            for (int i=0;i<3;++i)
            {
                // v1 v2 f ei 
                int v1 = F(f,i);
                int v2 = F(f,(i+1)%3);
                if (v1 > v2) std::swap(v1,v2);
                vector<int> r(4);
                r[0] = v1; r[1] = v2;
                r[2] = f;  r[3] = i;
                TTT.push_back(r);
            }
        std::sort(TTT.begin(),TTT.end());
        TT = MatrixXi::Constant((int)(F.rows()),3,-1);
        
        for(unsigned int i=2;i<TTT.size();++i)
        {
            vector<int>& r1 = TTT[i-2];
            vector<int>& r2 = TTT[i-1];
            vector<int>& r3 = TTT[i];
            if ( (r1[0] == r2[0] && r2[0] == r3[0]) 
                 && 
                 (r1[1] == r2[1] && r2[1] == r3[1]) )
            {
                return false;
            }
        }
        return true;
    }
    
	
    // Initialize TT-VT*
    void VF::initTT()
    {
        assert(isManifold());
        VT.resize(V.rows());
        vector<vector<int> > TTT;
        for(int f=0;f<F.rows();++f)
            for (int i=0;i<3;++i)
            {
                // VT*
                VT(F(f,i))=f;
                // v1 v2 f ei 
                int v1 = F(f,i);
                int v2 = F(f,(i+1)%3);
                if (v1 > v2) std::swap(v1,v2);
                vector<int> r(4);
                r[0] = v1; r[1] = v2;
                r[2] = f;  r[3] = i;
                TTT.push_back(r);
            }
        std::sort(TTT.begin(),TTT.end());
        TT = MatrixXi::Constant((int)(F.rows()),3,-1);
        
        for(unsigned int i=1;i<TTT.size();++i)
        {
            vector<int>& r1 = TTT[i-1];
            vector<int>& r2 = TTT[i];
            if ((r1[0] == r2[0]) && (r1[1] == r2[1]))
            {
                TT(r1[2],r1[3]) = r2[2];
                TT(r2[2],r2[3]) = r1[2];
            }
        }
        TTVTflag = true;
    }
    
        
    inline Vector3i VF::getTT(int i) // returns TT relation of the i-th face
    { 
        if (!TTVTflag) initTT();
        return TT.row(i);
    }
    
    VectorXi VF::getVT(int i) // returns VT relation of the i-th vertex
    {
        VectorXi buf(20); // fragile - no more than 20 neighbors!
        int curt, firstt, lastt, k=0;
        bool rounded = false;
        
        if (!TTVTflag) initTT(); 
        
        firstt = curt = VT(i);
        do {
            buf(k++) = curt;
            assert(k<20);
            for (int j=0;j<3;++j)
                if (F(curt,j)==i)
                {
                	lastt = curt;
                    curt = TT(curt,(j+2)%3);
                    break;
                }
            if (curt==firstt)
            {
            	rounded = true;
            }
        } while (curt!=firstt);
		if (!rounded) // TODO not tested
		{
			firstt = curt = VT(i);
		    do {
		        buf(k++) = curt;
		        assert(k<20);
		        for (int j=0;j<3;++j)
		            if (F(curt,j)==i)
		            {
		                curt = TT(curt,j);
		                break;
		            }
		    } while (curt!=lastt && curt!=firstt);
		}
        buf.conservativeResize(k);
        return buf;
    }
    
    VectorXi VF::getVV(int i) // returns VT relation of the i-th vertex
    {
        VectorXi buf(20); // fragile - no more than 20 neighbors!
        int curt, firstt, lastt, k=0;
        bool rounded = false;
        
        if (!TTVTflag) initTT(); 
        
        firstt = curt = VT(i);
        do {
	        if (curt<0) // TODO this eliminates a bug, but it's correct? It seems not.
	        	break;
            assert(k<20);
            for (int j=0;j<3;++j)
                if (F(curt,j)==i)
                {
                    buf(k++)= F(curt,(j+2)%3);
                    lastt = curt;
                    curt = TT(curt,(j+2)%3);
                    break;
                }
            if (curt==firstt)
            {
            	rounded = true;
            }
        } while (curt!=firstt);
		if (!rounded) // TODO not tested
		{
			firstt = curt = VT(i);
		    do {
			    if (curt<0) // TODO this eliminates a bug, but it's correct? It seems not.
			    	break;
		        assert(k<20);
		        for (int j=0;j<3;++j)
		            if (F(curt,j)==i)
		            {
		                buf(k++)= F(curt,j);
		                curt = TT(curt,j);
		                break;
		            }
		    } while (curt!=lastt && curt!=firstt);
		}
        
        buf.conservativeResize(k);
        return buf;
    }
    
    VectorXi VF::getRadialVV(int i, int radius)
    {
    	int bufsize=V.rows();
    	VectorXi buf(bufsize), vv;
    	typedef std::pair<int,int> QueueEl;
    	std::list< QueueEl > queue_to_visit;
    	int k=0;
    	int queued_id=0;
		assert(utilitybits[queued_id].setAllMatrixTo(false));
		
    	queue_to_visit.push_back(QueueEl(i,0));
		utilitybits[queued_id].setBitAt(i,0,true);
    	
    	while (!queue_to_visit.empty())
    	{
			QueueEl visiting = queue_to_visit.front();
			queue_to_visit.pop_front();

			assert(k<bufsize);
			if (visiting.first!=i)
			{
				buf(k++)=visiting.first;
			}
//			std::cout<<"visiting "<<visiting.first<<std::endl;
			if (visiting.second < radius)
			{
				vv = getVV(visiting.first);
//				std::cout<<vv.rows()<<" vertices around "<<visiting.first<<std::endl;
				int nr = visiting.second+1;
				for (int id=0 ; id<vv.rows() ; id++)
				{
					int v = vv(id);
					if (!utilitybits[queued_id].getBitAt(v,0))
					{
//						std::cout<<v<<" inserted"<<std::endl;
						queue_to_visit.push_back(QueueEl(v,nr));
						utilitybits[queued_id].setBitAt(v,0,true);
					}
				}
			}
		}
		utilitybits[queued_id].setAllMatrixTo(false);

        buf.conservativeResize(k);
        return buf;
    }
    
    void VF::nearestVertex(const Vec3& point, int& vertex_id)
    {
    	float min_dist = FLT_MAX;
    	Vec3 vp(point.getX(),point.getY(),point.getZ());
    	for (int i=0 ; i<V.rows() ; i++)
    	{
    		float nd = (Vec3(V(i,0),V(i,1),V(i,2))-vp).getModule();
    		if (nd<min_dist)
    		{
    			min_dist = nd;
    			vertex_id = i;
    		}
    	}
    }
    
    
    
    
//////////////////// OpenGL /////////////////////////////////////

	bool VF::setNonBaseColor(int c_radius, int v_id, ColorRenderType color_type)
	{
		if (!(c_radius!=-1 && c_radius<radiuses_num))
		{
			return false;
		}
		float* vdata = curvatures[c_radius][v_id];
		if (color_type==CURVATURE_COLOR)
		{
			glColor3fv(&vdata[CURV_COLOR_RED]);
			return true;
		} else
		if (color_type==VERTEX_TYPE_COLOR)
		{
			glColor3fv(getColorForVertexCurvType((VertexCurvType)((struct CBF*)&vdata[CURV_BITFIELD_DATA])->type));
			return true;
		}
		return false;
	}
	void VF::setBaseColor(void)
	{
		glColor3fv(VF::base_color_3f);
	}

    void VF::draw (draw_mode_t mode, bool light)
    {
        
		glColor3f (0.0,0.0,0.0);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef (-center[0], -center[1], -center[2]);
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview_matrix);

		if (mode == SMOOTH || mode == FLAT)
		{
			glEnable (GL_DEPTH_TEST);
			if (light)
			{
				glEnable(GL_LIGHTING);
			} else
			{
				glDisable(GL_LIGHTING);
			}
			if (mode == SMOOTH)
				glShadeModel(GL_SMOOTH);
			if (mode == FLAT)
				glShadeModel(GL_FLAT);
	
			glBegin (GL_TRIANGLES);
			for (int i = 0; i < F.rows(); i++)
			{
				if (F.cols() != 3 || V.cols() != 3)
					fprintf (stderr, "VF:draw(): only triangles in 3D are supported\n");

				int i0,i1,i2;
				i0 = i1 = i2 = 0;
				if (i0 < 0 || i0 >= V.rows())
					fprintf (stderr, "VF::draw(): out of boundary F(%d,%d) = %d\n",
						 i,0,F(i,0));
				if (i1 < 0 || i1 >= V.rows())
					fprintf (stderr, "VF::draw(): out of boundary F(%d,%d) = %d\n",
						 i,1,F(i,1));
				if (i2 < 0 || i2 >= V.rows())
					fprintf (stderr, "VF::draw(): out of boundary F(%d,%d) = %d\n",
						 i,2,F(i,2));
				i0 = F(i,0);
				i1 = F(i,1);
				i2 = F(i,2);
				
				Vector3d v0 (V(i0,0), V(i0,1), V(i0,2));
				Vector3d v1 (V(i1,0), V(i1,1), V(i1,2));
				Vector3d v2 (V(i2,0), V(i2,1), V(i2,2));

				// normal
				Vector3d u,v,n;
				u = v1 - v0;
				v = v2 - v0;
				n = (u.cross(v));

				glNormal3f (n(0), n(1), n(2));

				// triangle
				// per vertex normals should be computed eventually.
//				    	Vec3 curv_dir_min(curvatures[active_curvature][i0][CURV_MINCURV];
				bool unused_bool;
				if (!setNonBaseColor(active_curvature, i0, color_type))
				{
					setBaseColor();
				}
				glVertex3f (v0(0), v0(1), v0(2));
				
				unused_bool = setNonBaseColor(active_curvature, i1, color_type);
				glVertex3f (v1(0), v1(1), v1(2));
				
				unused_bool = setNonBaseColor(active_curvature, i2, color_type);
				glVertex3f (v2(0), v2(1), v2(2));
			}
			glEnd();			
		}

		if (mode == POINTS)
		{
			glDisable (GL_DEPTH_TEST);
			glDisable(GL_LIGHTING);
			glBegin (GL_POINTS);

			for (int i = 0; i < V.rows(); i++)
			{
				if (!setNonBaseColor(active_curvature, i, color_type))
				{
					setBaseColor();
				}
				glVertex3f (V(i,0), V(i,1), V(i,2));
			}
			glEnd();				    
		}

		if (mode == WIRE)
		{
			glDisable (GL_DEPTH_TEST);
			glDisable (GL_LIGHTING);
			glShadeModel(GL_SMOOTH);
			setBaseColor();
			
			bool unused_bool;
			glBegin (GL_LINES);
			for (int i = 0; i < F.rows(); i++)
			{
				int i0,i1,i2;
				i0 = i1 = i2 = 0;

				i0 = F(i,0);
				i1 = F(i,1);
				i2 = F(i,2);
		
				Vector3d v0 (V(i0,0), V(i0,1), V(i0,2));
				Vector3d v1 (V(i1,0), V(i1,1), V(i1,2));
				Vector3d v2 (V(i2,0), V(i2,1), V(i2,2));

				// triangle
				
				unused_bool = setNonBaseColor(active_curvature, i0, color_type);
				glVertex3f (v0(0), v0(1), v0(2));
				
				unused_bool = setNonBaseColor(active_curvature, i1, color_type);
				glVertex3f (v1(0), v1(1), v1(2));
				
				unused_bool = setNonBaseColor(active_curvature, i0, color_type);
				glVertex3f (v0(0), v0(1), v0(2));
				
				unused_bool = setNonBaseColor(active_curvature, i2, color_type);
				glVertex3f (v2(0), v2(1), v2(2));
				
				unused_bool = setNonBaseColor(active_curvature, i1, color_type);
				glVertex3f (v1(0), v1(1), v1(2));
				
				unused_bool = setNonBaseColor(active_curvature, i2, color_type);
				glVertex3f (v2(0), v2(1), v2(2));
			}
			glEnd();			
		}
	
		if (render_features && active_curvature>=0 && active_curvature<radiuses_num)
		{
			if ( render_nonmaximasuppressed && !non_maxima_suppression_calculated.getBitAt(active_curvature,0))
			{
				std::cout<<"non maxima suppression on radius id "<<active_curvature<<" ("<<radiuses[active_curvature]<<")"<<std::endl;
				nonMaximaSuppression(active_curvature);
				std::cout<<"	done"<<std::endl;
			}
			bool unused_bool;
	//		glDisable (GL_DEPTH_TEST);
			glDisable(GL_LIGHTING);
			GLfloat old_point_size;
			glGetFloatv(GL_POINT_SIZE,&old_point_size);
			glPointSize(5.0f);
			glBegin (GL_POINTS);
			for (int i = 0; i < V.rows(); i++)
			{
				float* vdata = curvatures[active_curvature][i];
				if (!render_features_of_type[((struct CBF*)&vdata[CURV_BITFIELD_DATA])->type] || (render_nonmaximasuppressed && ((struct CBF*)&vdata[CURV_BITFIELD_DATA])->is_maximum == 0x0))
				{
					continue;
				}
				
				unused_bool = setNonBaseColor(active_curvature, i, VERTEX_TYPE_COLOR);
				glVertex3f (V(i,0), V(i,1), V(i,2));
			}
			glEnd();
			glPointSize(old_point_size);
		}
	
		if (render_radius_test)
		{
			int testv = -1;
			nearestVertex(test_picked_point, testv);
			if (testv!=-1)
			{
				VectorXi vv = getRadialVV(testv, getNonMaximaSuppressionRadius(active_curvature));
				glDisable(GL_LIGHTING);
				GLfloat old_point_size;
				glGetFloatv(GL_POINT_SIZE,&old_point_size);
				glPointSize(4.0f);
				glBegin (GL_POINTS);
				glColor3f(1.0f, 0.5f, 0.0f);
				for (int i=0 ; i<vv.rows() ; i++)
				{
					glVertex3f(V(vv(i),0), V(vv(i),1), V(vv(i),2));
				}
				glColor3f(1,0,1);
				glVertex3f(V(testv,0), V(testv,1), V(testv,2));
				glEnd();
				glPointSize(old_point_size);
			}
		}

		glPopMatrix();
		return;	
    }

    void VF::bb ()
    {
        // FE = MatrixXi::Constant((int)(F.rows()),3,-1);
	VectorXd min, max;
	VectorXd vec;

	min = VectorXd::Constant(V.cols(),0.0);
	max = VectorXd::Constant(V.cols(),0.0);
	//center = VectorXd::Constant(V.cols(),0.0);
	center[0] = center[1] = center[2] = 0.0;

	min = V.colwise().minCoeff();
	max = V.colwise().maxCoeff();

	vec = ((max - min) / 2.0);
	vec = (V.colwise().sum()) / (float)(V.rows());
	// for (unsigned i = 0; i < 3; i++)
	//     center(i) = vec(i);
	center[0] = vec(0);
	center[1] = vec(1);
	center[2] = vec(2);
	// center[0] = center[1] = center[2] = 0.0;
	
	fprintf (stdout, "mesh center: %f %f %f.\n", center[0], center[1], center[2]);
	diagonal = (max - min).norm();
	std::cout << "mesh diagonal: " << diagonal << "." << std::endl;
    }
