/* 
 * File:   BitMatrix2D.cpp
 * Author: thegiallo
 * 
 * Created on 28 novembre 2011, 13.14
 */

#include <cstdint>
#include <cstring>
#include <cstdlib>

#include "BitMatrix2D.h"
#include <iostream>


BitMatrix2D::BitMatrix2D()
{
    blob = NULL;
    this->width = 0;
    this->height = 0;
}


BitMatrix2D::BitMatrix2D(unsigned int width, unsigned int height)
{
    blob = NULL;
    initBlob(width, height);
    if (blob==NULL)
    {
    	std::cerr<<"error allocating memory for "<<SIZEOFBLOB<<"bytes"<<std::endl;
    	exit(1);
    }
    setAllMatrixTo(false);
}

BitMatrix2D::BitMatrix2D(const BitMatrix2D& orig)
{
    blob = NULL;
    initBlob(orig.width, orig.height);
    memccpy(this->blob, orig.blob, SIZEOFBLOB, sizeof(uint_fast8_t));
}

BitMatrix2D::~BitMatrix2D()
{
	if (blob!=NULL)
    {
//    	free(blob); //commented because do give error... ???
	}
}

bool BitMatrix2D::initBlob(unsigned int width, unsigned int height)
{
    if (blob!=NULL)
    {
        return false;
    }
    if (width==0 || height==0)
    {
    	std::cerr<<"initializing a blob with 0 dimension"<<std::endl;
    	throw false;
    }
    this->width = width;
    this->height = height;
    blob = (uint_fast8_t*)malloc(SIZEOFBLOB);
    return true;
}

bool BitMatrix2D::initBlob(unsigned int width, unsigned int height, uint_fast8_t* blob)
{
    initBlob(width, height);
/*    if (sizeof(blob)/sizeof(uint_fast8_t)!=SIZEOFBLOB)
    {
        throw false;
    }*/
    memcpy(this->blob,blob,SIZEOFBLOB);
}
bool BitMatrix2D::loadFrom(unsigned int width, unsigned int height, uint_fast8_t* bytes_array)
{
    initBlob(width, height);
    if (this->width!=width && this->height!=height)
    {
        return false;
    }
    /*for (unsigned int j=0 ; j<height ; j++)
    {
        for (unsigned int i=0 ; i<width ; i++)
        {
            setBitAt(i,j,bytes_array[j*width+i]);
        }
    }*/
    memcpy(this->blob,bytes_array,SIZEOFBLOB);
    return true;
}
bool BitMatrix2D::writeTo(uint_fast8_t* bytes_array)
{
    if (blob==NULL)
    {
        return false;
    }
    for (unsigned int j=0 ; j<height ; j++)
    {
        for (unsigned int i=0 ; i<width ; i++)
        {
            bytes_array[j*width+i] = getBitAt(i,j)?0xFF:0x00;
        }
    }
    return true;
}
bool BitMatrix2D::setAllMatrixTo(bool bit)
{
    if (blob==NULL)
    {
        return false;
    }
    memset(blob, bit?0xFF:0, SIZEOFBLOB);
    return true;
}

bool BitMatrix2D::getBitAt(unsigned int x, unsigned int y)
{
    if (x>=this->width || y>=this->height)
    {
    #ifdef DEBUG
    	std::cerr<<"coordinates out of matrix in getBitAt"<<std::endl;
    	std::cerr<<"	width:"<<width<<" height:"<<height<<" x:"<<x<<" y:"<<y<<std::endl;
	#endif
        throw false;
    }
    unsigned int nbit = y*width+x;
    unsigned int nbyte = nbit/8;
    unsigned int nbit_in_byte = 7-nbit%8;
    return (blob[nbyte]&(0x1<<nbit_in_byte))!=0;
}
/**
 *  set the element with pos x,y to bit
 * @param x coord on width
 * @param y coord on height
 * @param bit the new value
 * @return the old value
 */
bool BitMatrix2D::setBitAt(unsigned int x, unsigned int y, bool bit)
{
    if (x>=this->width || y>=this->height)
    {
    #ifdef DEBUG
    	std::cerr<<"coordinates out of matrix in setBitAt"<<std::endl;
    	std::cerr<<"	width:"<<width<<" height:"<<height<<" x:"<<x<<" y:"<<y<<std::endl;
        throw false;
    #endif
    }
    unsigned int nbit = y*width+x;
    unsigned int nbyte = nbit/8;
    unsigned int nbit_in_byte = 7-nbit%8;
    bool res = (blob[nbyte]&(0x1<<nbit_in_byte))!=0;
    blob[nbyte] = (blob[nbyte]&~(0x1<<nbit_in_byte));
    if (bit)
    {
        blob[nbyte] |= 0x1<<nbit_in_byte;
    }
    return res;
}
unsigned int BitMatrix2D::getWidth()
{
    return width;
}
unsigned int BitMatrix2D::getHeight()
{
    return height;
}
unsigned int BitMatrix2D::getSizeOfBlob(void)
{
    return SIZEOFBLOB;
}
unsigned int BitMatrix2D::getSizeOfBlob(unsigned int width, unsigned int height)
{
	return SIZEOFBLOB;
}
