#include <string.h>

#include "Matrix4.h"

Matrix4::Matrix4()
{
    memset(matrix[0],0,sizeof(float)*4);
    memset(matrix[1],0,sizeof(float)*4);
    memset(matrix[2],0,sizeof(float)*4);
    memset(matrix[3],0,sizeof(float)*4);
}
Matrix4::Matrix4(  float a00, float a01, float a02, float a03,
                                float a10, float a11, float a12, float a13,
                                float a20, float a21, float a22, float a23,
                                float a30, float a31, float a32, float a33 )
{
    matrix[0][0] = a00;
    matrix[0][1] = a01;
    matrix[0][2] = a02;
    matrix[0][3] = a03;

    matrix[1][0] = a10;
    matrix[1][1] = a11;
    matrix[1][2] = a12;
    matrix[1][3] = a13;

    matrix[2][0] = a20;
    matrix[2][1] = a21;
    matrix[2][2] = a22;
    matrix[2][3] = a23;

    matrix[3][0] = a30;
    matrix[3][1] = a31;
    matrix[3][2] = a32;
    matrix[3][3] = a33;
}
Matrix4::~Matrix4()
{

}
float** Matrix4::getMatrix(void)
{
    float **ret = new float*[4];
    for (int i=0; i<4; i++)
    {
        ret[i]=new float[4];
    }
    memcpy(ret[0],matrix[0],sizeof(float)*4);
    memcpy(ret[1],matrix[1],sizeof(float)*4);
    memcpy(ret[2],matrix[2],sizeof(float)*4);
    memcpy(ret[3],matrix[3],sizeof(float)*4);
    return ret;
}
void Matrix4::set(uint8_t i, uint8_t j, float val)
{
    matrix[i][j] = val;
}
float Matrix4::get(uint8_t i, uint8_t j)
{
    return matrix[i][j];
}
