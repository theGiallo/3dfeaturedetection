#include <math.h>
#include <iostream>

#include "Vec3.h"

Vec3::Vec3()
{
    x = y = z = 0;
}
Vec3::Vec3(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}
float Vec3::getModule(void)
{
    return sqrt(x*x+y*y+z*z);
}
float Vec3::getSin(Vec3 v)
{
    return (this->getNormalized()^v.getNormalized()).getModule();
}
float Vec3::getCos(Vec3 v)
{
    return this->getNormalized()*v.getNormalized();
}
void Vec3::normalize(void)
{
    float l = this->getModule();
    if ( l != 0 )
    {
        *this /= l;
    }
}
Vec3 Vec3::getNormalized(void)
{
    Vec3 tmp = getClone();
    tmp.normalize();
    return tmp;
}
void Vec3::add(Vec3 v)
{
    x+=v.x;
    y+=v.y;
    z+=v.z;
}
Vec3 Vec3::operator +(Vec3 v)
{
    Vec3 tmp = this->getClone();
    tmp.add(v);
    return tmp;
}
Vec3 Vec3::operator +=(Vec3 v)
{
    this->add(v);
    return *this;
}
void Vec3::sub(Vec3 v)
{
    x-=v.x;
    y-=v.y;
    z-=v.z;
}
Vec3 Vec3::operator -(Vec3 v)
{
    Vec3 tmp = this->getClone();
    tmp.sub(v);
    return tmp;
}
Vec3 Vec3::operator -=(Vec3 v)
{
    this->sub(v);
    return *this;
}
void Vec3::add(uint8_t i, float f)
{
    switch(i%3)
    {
        case X_COMPONENT:
            x += f;
            break;
        case Y_COMPONENT:
            y += f;
            break;
        case Z_COMPONENT:
            z += f;
            break;
    }
}
void Vec3::add(float k)
{
    x+=k;
    y+=k;
    z+=k;
}
Vec3 Vec3::operator +(float k)
{
    Vec3 tmp = this->getClone();
    tmp.add(k);
    return tmp;
}
Vec3 Vec3::operator +=(float k)
{
    this->add(k);
    return *this;
}
void Vec3::sub(float k)
{
    x-=k;
    y-=k;
    z-=k;
}
Vec3 Vec3::operator -(float k)
{
    Vec3 tmp = this->getClone();
    tmp.sub(k);
    return tmp;
}
Vec3 Vec3::operator -=(float k)
{
    this->sub(k);
    return *this;
}
void Vec3::mult(uint8_t i, float f)
{
    switch(i%3)
    {
        case X_COMPONENT:
            x *= f;
            break;
        case Y_COMPONENT:
            y *= f;
            break;
        case Z_COMPONENT:
            z *= f;
            break;
    }
}
void Vec3::mult(float k)
{
    x*=k;
    y*=k;
    z*=k;
}
Vec3 Vec3::operator *(float k)
{
    Vec3 tmp = this->getClone();
    tmp.mult(k);
    return tmp;
}
Vec3 Vec3::operator *=(float k)
{
    this->mult(k);
    return *this;
}
void Vec3::div(uint8_t i, float f)
{
    switch(i%3)
    {
        case X_COMPONENT:
            x /= f;
            break;
        case Y_COMPONENT:
            y /= f;
            break;
        case Z_COMPONENT:
            z /= f;
            break;
    }
}
void Vec3::div(float k)
{
    if (k==0)
        return;
    x/=k;
    y/=k;
    z/=k;
}
Vec3 Vec3::operator/(float k)
{
    Vec3 tmp = this->getClone();
    tmp.div(k);
    return tmp;
}
Vec3 Vec3::operator/=(float k)
{
    this->div(k);
    return *this;
}
float Vec3::scalar(Vec3 v)
{
    return (x*v.x+y*v.y+z*v.z);
}
float Vec3::operator *(Vec3 v)
{
    return this->scalar(v);
}
Vec3 Vec3::getVectorial(Vec3 v)
{
    return Vec3( y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x );
}
Vec3 Vec3::operator ^(Vec3 v)
{
    return this->getVectorial(v);
}
Vec3 Vec3::operator ^=(Vec3 v)
{
    *this = this->getVectorial(v);
	return *this;
}
bool Vec3::isEqual(Vec3 v)
{
    return x==v.x&&
                y==v.y&&
                z==v.z;
}
bool  Vec3::operator ==(Vec3 v)
{
    return this->isEqual(v);
}
bool  Vec3::operator !=(Vec3 v)
{
    return !(this->isEqual(v));
}
void  Vec3::clone(Vec3 v)
{
    x = v.x;
    y = v.y;
    z = v.z;
}
Vec3  Vec3::getClone(void)
{
    return Vec3(x,y,z);
}
Vec3  Vec3::operator =(Vec3 v)
{
    this->clone(v);
    return *this;
}
float Vec3::get(uint8_t i) const
{
    switch(i%3)
    {
        case X_COMPONENT:
            return x;
        case Y_COMPONENT:
            return y;
        case Z_COMPONENT:
            return z;
    }
}
float  Vec3::getX(void) const
{
    return x;
}
float  Vec3::getY(void) const
{
    return y;
}
float  Vec3::getZ(void) const
{
    return z;
}
void Vec3::set(uint8_t i, float f)
{
    switch(i%3)
    {
        case X_COMPONENT:
            x = f;
        case Y_COMPONENT:
            y = f;
        case Z_COMPONENT:
            z = f;
    }
}
void  Vec3::setX(float x)
{
    this->x = x;
}
void  Vec3::setY(float y)
{
    this->y = y;
}
void  Vec3::setZ(float z)
{
    this->z = z;
}
void Vec3::print()
{
    std::cout<<"("<<x<<", "<<y<<", "<<z<<")"<<std::endl;
}

void Vec3::rotate(Quaternion q, Vec3 center)
{;
    *this = q*(*this - center) + center;
}
Vec3 Vec3::getRotated(Quaternion q, Vec3 center)
{
    Vec3 tmp = *this;
    tmp.rotate(q,center);
    return tmp;
}
void Vec3::scale(Vec3 s, Vec3 center)
{
    x = (x-center.x)*s.x+center.x;
    y = (y-center.y)*s.y+center.y;
    z = (z-center.z)*s.z+center.z;
}
Vec3 Vec3::getScaled(Vec3 s, Vec3 center)
{
    Vec3 tmp = *this;
    tmp.scale(s,center);
    return tmp;
}
Vec3::operator Vec3_int()
{
    return Vec3_int(x,y,z);
}


Vec3_int::Vec3_int()
{
    x=y=z=0;
}
Vec3_int::Vec3_int(int x, int y, int z)
{
    this->x=x;
    this->y=y;
    this->z=z;
}
Vec3_int::Vec3_int(int* xyz)
{
    this->x=xyz[X_COMPONENT];
    this->y=xyz[Y_COMPONENT];
    this->z=xyz[Z_COMPONENT];
}

Vec3_int::operator Vec3()
{
    return Vec3(x,y,z);
}
Vec3_int Vec3_int::operator +(Vec3_int v)
{
    return Vec3_int(x+v.x,y+v.y,z+v.z);
}
Vec3_int Vec3_int::operator +=(Vec3_int v)
{
    x+=v.x;
    y+=v.y;
    z+=v.z;
    return *this;
}
Vec3_int Vec3_int::operator -(Vec3_int v)
{
    return Vec3_int(x-v.x,y-v.y,z-v.z);
}
Vec3_int Vec3_int::operator -=(Vec3_int v)
{
    x-=v.x;
    y-=v.y;
    z-=v.z;
    return *this;
}
bool Vec3_int::operator ==(Vec3_int v)
{
    return x==v.x&&y==v.y&&z==v.z;
}
bool Vec3_int::operator !=(Vec3_int v)
{
    return x!=v.x||y!=v.y||z!=v.z;
}
void Vec3_int::print()
{
    std::cout<<"("<<x<<", "<<y<<", "<<z<<")";
}
