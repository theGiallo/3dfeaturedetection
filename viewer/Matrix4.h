/* 
 * File:   Matrix4.h
 * Author: thegiallo
 *
 * Created on 29 gennaio 2011, 15.44
 */

#ifndef MATRIX4_H
#define	MATRIX4_H

#include <cstdint>

class Matrix4
{
protected:
    float matrix[4][4];
public:
    Matrix4(void);
    Matrix4(  float a00, float a01, float a02, float a03,
                    float a10, float a11, float a12, float a13,
                    float a20, float a21, float a22, float a23,
                    float a30, float a31, float a32, float a33 );
    ~Matrix4(void);
    float** getMatrix(void);
    void set(uint8_t i, uint8_t j, float val);
    float get(uint8_t i, uint8_t j);
};

#endif	/* MATRIX4_H */

