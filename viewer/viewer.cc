// opengl
// #include <GL/glew.h>
#ifdef __APPLE__
#include <glut.h>
#else
#include <GL/glut.h>
#endif
#define __ESC_GLUT 27

#include <AntTweakBar.h>

#include "VF.hh"
#include "camera.hh"
//#include "utility.hh"
//#include "GLutils.hh"
//#include "cotmatrix_addins/cotmatrix.h"
//#include "cotmatrix_addins/massmatrix.h"
//#include <Eigen/SparseCholesky>

/////////////
// GLOBALS //
/////////////

#define W 800
#define H 600

// OpenGl related vars
Camera camera;
int width = W;
int height = H;

VF::draw_mode_t mesh_draw_mode = VF::SMOOTH;
#define LIGHTING_MODE_NUM 4
#define LIGHTING_NONE 0
#define LIGHTING_NO_REFLEXES 1
#define LIGHTING_REFLEXES 2
#define LIGHTING_MATTE 3
#define DEFAULT_LIGHT_MODE LIGHTING_NO_REFLEXES

#define CLEAR_COLOR_NUM 2
#define BLACK 0
#define WHITE 1

float black_3[3] = {0.0f,0.0f,0.0f};
float white_3[3] = {1.0f,1.0f,1.0f};
float* clear_color = black_3;

enum MESH_TO_DISPLAY_TYPE {ORIGINAL, SMOOTHED, SMOOTHEDITED, EDITED};
MESH_TO_DISPLAY_TYPE DisplayMode= ORIGINAL;

// Mesh:
VF m;


//////////
// QUIT //
//////////

void quit()
{
    // insert here eventual clean up code
    exit(0);
}


///////////////////
// GLUT CALLBACK //
///////////////////

void display()
{
    glClearColor(clear_color[0], clear_color[1], clear_color[2], 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m.test_picked_point = Vec3(camera.last_picked_point[0],camera.last_picked_point[1],camera.last_picked_point[2]);
	
    // draw mesh
    camera.display_begin();
    m.draw(mesh_draw_mode, camera.lighting_mode!=LIGHTING_NONE);
    camera.display_end();
    
    // draw GUI
    TwDraw();

    glFlush();
    glutSwapBuffers();
}

void reshape(int w, int h)
{
    camera.reshape(w,h);

    width = w;
    height = h;
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);

    TwWindowSize(w, h);

    glutPostRedisplay();
}

void mouse_click(int button, int state, int x, int y)
{
    if (TwEventMouseButtonGLUT(button, state, x, y))
    {
        glutPostRedisplay();
        return;
    }

	// insert here management of selection and editing
	
	// camera motion with left button
	camera.mouse(button, state, x, y);
    glutPostRedisplay();
}


void mouse_move(int x, int y)
{
    if (TwEventMouseMotionGLUT(x, y))
    {
        glutPostRedisplay();
        return;
    }
	
	// insert here editing and selection management

    else // view motion
        camera.mouse_move(x,y);
    glutPostRedisplay();
}


void keyboard(unsigned char k, int x, int y)
{
    if (k == __ESC_GLUT) quit();

    TwEventKeyboardGLUT(k, x, y);
	
    // insert here management of other keybord key press 

    //camera.key (k, x, y);
    glutPostRedisplay();
}

void special(int k, int x, int y)
{
    TwEventSpecialGLUT(k, x, y);

    //camera.special (k, x, y);
    glutPostRedisplay();
}


///////////////////////////
// ANTTWEAKBAR CALLBACKS //
///////////////////////////

void TW_CALL setAperture (const void *value, void *)
{
    camera.gCamera.aperture = *(const double *) value;
    glutPostRedisplay();
}

void TW_CALL getAperture (void *value, void *)
{
    *(double *) value = camera.gCamera.aperture;
}

void TW_CALL setFocalLength (const void *value, void *)
{
    camera.gCamera.focalLength = *(const double *) value;
    glutPostRedisplay();
}

void TW_CALL getFocalLength (void *value, void *)
{
    *(double *) value = camera.gCamera.focalLength;
}

void TW_CALL call_quit(void *clientData)
{
	int cd = *(int*)clientData;
	cd++;
    quit();
}


void TW_CALL setColorMode (const void *value, void *)
{
    if (m.color_type!=*(const ColorRenderType*)value)
    {
    	m.color_type = *(const ColorRenderType*) value;
		glutPostRedisplay();
	}
}
void TW_CALL getColorMode (void *value, void *)
{
    *(ColorRenderType*) value = m.color_type;
}

void TW_CALL setLightMode (const void *value, void *)
{
    if (camera.lighting_mode!=*(const unsigned int*)value)
    {
    	camera.lighting_mode = *(const unsigned int*) value;
    	camera.SetLighting(camera.lighting_mode);
	    glutPostRedisplay();
	}
}
void TW_CALL getLightMode (void *value, void *)
{
    *(unsigned int*) value = camera.lighting_mode;
}
void TW_CALL setCameraLight (const void *value, void *)
{
    if (camera.camera_light!=*(const unsigned int*)value)
    {
    	camera.camera_light = *(const unsigned int*) value;
    	camera.SetLighting(camera.lighting_mode);
	    glutPostRedisplay();
	}
}
void TW_CALL getCameraLight (void *value, void *)
{
    *(unsigned int*) value = camera.camera_light;
}

void TW_CALL setClearColor (const void *value, void *)
{
	switch(*(int*)value)
	{
	case BLACK:
		clear_color = black_3;
		break;
	case WHITE:
		clear_color = white_3;
		break;
	}
}
void TW_CALL getClearColor (void *value, void *)
{
	if (clear_color==black_3)
    {
    	*(unsigned int*) value = BLACK;
	} else
	if (clear_color==white_3)
    {
    	*(unsigned int*) value = WHITE;
	}
}

void setCurvEps(const void*value, void*)
{
	VF::curv_eps = *(const float*)value;
	m.calcVertexCurvType();
	m.non_maxima_suppression_calculated.setAllMatrixTo(false);
}
void getCurvEps(void* value, void*)
{
	*(float*)value = VF::curv_eps;
}

void setNMSrf(const void*value, void*)
{
	m.NMS_radius_factor = *(const float*)value;
	m.calcVertexCurvType();
	m.non_maxima_suppression_calculated.setAllMatrixTo(false);
}
void getNMSrf(void* value, void*)
{
	*(float*)value = m.NMS_radius_factor;
}
void getCurvatureRadius(void* value, void*)
{
	*(int*)value = m.active_curvature==-1?-1: m.radiuses[m.active_curvature];
}


//////////
// MAIN //
//////////

int main (int argc, char *argv[])
{
    if (argc != 3)
    {
	fprintf (stderr, "Usage: ./viewer mesh_file curvatures_dir_path\n");
	exit (-1);
    }
	m.curvatures_dir_path = std::string(argv[2]);
    m.read(argv[1]);
    m.bb();
    
    m.modelview_matrix = camera.modelview_matrix;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(W, H);
    glutCreateWindow("viewer");

    TwInit(TW_OPENGL, NULL);
    TwWindowSize(W, H);

    glClearColor(0, 0, 0, 0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    //glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);

    camera.SetLighting(DEFAULT_LIGHT_MODE);
    camera.gCameraReset(m.diagonal, m.center);

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse_click);
    glutMotionFunc(mouse_move);
    glutPassiveMotionFunc(mouse_move);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    TwGLUTModifiersFunc(glutGetModifiers);

    TwBar *cBar;
    cBar = TwNewBar("Camera_Rendering");
    TwDefine("Camera_Rendering size='250 300'");
    TwDefine("Camera_Rendering valueswidth=80");
    TwDefine("Camera_Rendering color='192 255 192' text=dark ");

    TwAddButton(cBar, "Quit", call_quit, NULL, "");

    TwEnumVal draw_modeEV[VF::VF_DRAW_MODE_NUM] = {
        { VF::SMOOTH, "Smooth"}, {VF::FLAT, "Flat" }, { VF::POINTS, "Points" }, { VF::WIRE, "Wire" } }; 
        // { VF::HIDDEN, "Hidden" }, { VF::FLAT, "Flat" }};
    TwType draw_modeT = TwDefineEnum("drawModeType", draw_modeEV, VF::VF_DRAW_MODE_NUM);
    TwAddVarRW(cBar, "DrawMode", draw_modeT, &mesh_draw_mode,
	       "group = 'Scene'" " keyIncr='<' keyDecr='>'"); 
	       
    TwEnumVal light_modeEV[LIGHTING_MODE_NUM] = {
        { LIGHTING_NONE, "None" }, { LIGHTING_NO_REFLEXES, "No Reflexes" }, { LIGHTING_REFLEXES, "Reflexes" }, { LIGHTING_MATTE, "Matte" } }; 
    TwType light_modeT = TwDefineEnum("lightModeType", light_modeEV, LIGHTING_MODE_NUM);
    TwAddVarCB(cBar, "LightMode", light_modeT, setLightMode, getLightMode, NULL,
	       "group = 'Scene'"); 
	       
	       
    TwEnumVal clear_colorEV[CLEAR_COLOR_NUM] = {
        { BLACK, "Black" }, { WHITE, "White" }}; 
    TwType clear_colorT = TwDefineEnum("Background Color", clear_colorEV, CLEAR_COLOR_NUM);
    TwAddVarCB(cBar, "Background Color", clear_colorT, setClearColor, getClearColor, NULL,
	       "group = 'Scene'"); 

    // camera light
    TwAddVarCB(cBar, "camera light", TW_TYPE_BOOLCPP, setCameraLight, getCameraLight, NULL, "group = Scene");
	       
    // draw curvature colors
    TwEnumVal color_modeEV[COLOR_RENDER_TYPE_COUNT] = {
        { CURVATURE_COLOR, "Curvature Color" }, { VERTEX_TYPE_COLOR, "Vertex Type Color" },{BASE_COLOR, "Base Color"}}; 
    TwType color_modeT = TwDefineEnum("Color Mode", color_modeEV, COLOR_RENDER_TYPE_COUNT);
    TwAddVarCB(cBar, "color mode", color_modeT, setColorMode, getColorMode, NULL, "group='Curvature'");
    
	std::ostringstream ss;
  	ss<<"min=-1 max="<<m.radiuses_num-1<<" group='Curvature' keyIncr='+' keyDecr='-'";
  	std::string attr;
  	attr=ss.str();
  	std::cout<<"attr: "<<attr<<std::endl;
    // id of radius of curvature
    TwAddVarRW(cBar, "curvature radius id", TW_TYPE_INT32, &m.active_curvature, attr.c_str());
    // radius of curvature
    TwAddVarCB(cBar, "curvature radius", TW_TYPE_INT32, NULL, getCurvatureRadius, NULL, "group=Curvature readonly=true");
    // zero epsilon
    TwAddVarCB(cBar, "zero epsilon", TW_TYPE_FLOAT, setCurvEps, getCurvEps, NULL, "min=0.00 max=1.00 step=0.01 group = Curvature keyIncr='x' keyDecr='z'");
    // render feature points
    TwAddVarRW(cBar, "feature points", TW_TYPE_BOOLCPP, &m.render_features, "group = Curvature");
    // render non-maxima suppressed
    TwAddVarRW(cBar, "Non Maxima Suppression", TW_TYPE_BOOLCPP, &m.render_nonmaximasuppressed, "group = Curvature");
    // NMS radius factor
    TwAddVarCB(cBar, "NMS radius factor", TW_TYPE_FLOAT, setNMSrf, getNMSrf, NULL, "min=0 max=5 group = Curvature");
    // render test radius
    TwAddVarRW(cBar, "show NMS radius", TW_TYPE_BOOLCPP, &m.render_radius_test, "group = Curvature help='pick with CTRL+ALT+LMB'");
    
    // render feature points of type CONVEX_ELLIPTICAL
    TwAddVarRW(cBar, "convex elliptical", TW_TYPE_BOOLCPP, &m.render_features_of_type[CONVEX_ELLIPTICAL], "group = Curvature");
    // render feature points of type CONCAVE_ELLIPTICAL
    TwAddVarRW(cBar, "concave elliptical", TW_TYPE_BOOLCPP, &m.render_features_of_type[CONCAVE_ELLIPTICAL], "group = Curvature");
    // render feature points of type HYPERBOLIC
    TwAddVarRW(cBar, "hyperbolic", TW_TYPE_BOOLCPP, &m.render_features_of_type[HYPERBOLIC], "group = Curvature");
    // render feature points of type CONVEX_CYLINDRICAL
    TwAddVarRW(cBar, "convex cylindrical", TW_TYPE_BOOLCPP, &m.render_features_of_type[CONVEX_CYLINDRICAL], "group = Curvature");
    // render feature points of type CONCAVE_CYLINDRICAL
    TwAddVarRW(cBar, "concave cylindrical", TW_TYPE_BOOLCPP, &m.render_features_of_type[CONCAVE_CYLINDRICAL], "group = Curvature");
    // render feature points of type PLANAR
    TwAddVarRW(cBar, "planar", TW_TYPE_BOOLCPP, &m.render_features_of_type[PLANAR], "group = Curvature");

    // info
    TwAddVarRW(cBar, "show camera help", TW_TYPE_BOOLCPP, &camera.gShowHelp, "group = 'Scene'");
    
    // help
    TwAddVarRW(cBar, "show camera info", TW_TYPE_BOOLCPP, &camera.gShowInfo, "group = 'Scene'");

    // aperture
    TwAddVarCB(cBar, "camera aperture", TW_TYPE_DOUBLE, setAperture, getAperture,
	       NULL, "min=0.00 max=100.00 step=0.1");

    // focus distance
    TwAddVarCB(cBar, "camera focus", TW_TYPE_DOUBLE, setFocalLength, getFocalLength,
	       NULL, "min=0.00 max=100.00 step=0.1");

    glutMainLoop();
    exit (-1);
}
